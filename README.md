## Nationale Hypotheek Bond 
### Maximale hypotheek berekenen

Met deze tool kun je op basis van jouw **situatie**, **inkomen** en **verplichte kosten** jouw maximale hypotheek bedrag berekenen, door gebruik te maken van de Nationale Hypotheek Bond API. 


#### Requirements
- Laravel Valet (https://laravel.com/docs/8.x/valet)
- Node versie 16.0.0
- NPM versie 7.24.1

#### Installatie
1. Repository uitchecken in Valet folder
2. Commando's uitvoeren:
   1. `composer install`
   2. `npm install`
   3. `npm run dev`

3. Navigeren naar {jouw-folder}.test van het project staat
4. [Optioneel nodig] het aanpassen van de API-key in het .env bestand. 

De tool kan worden ingevuld.

#### Tests
Commando voor simpele browser-test*:

``php artisan dusk``

Commando voor een simpele feature-test, waarbij de POST naar verschillende methods gechecked word:

``php artisan test``

(* zorg dat je browser (Chrome) geupdate is naar de laatste versie)
