<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    //
    public function income_type(Request $request) {

        $index = $request->index;

        switch($request->income_type) {

            // employment
            case 1:
                return view('calculator.components.form-employment', compact('index'))->render();
                break;

            // social benefits
            case 2:
                return view('calculator.components.form-social-benefits', compact('index'))->render();
                break;
        }
    }
}
