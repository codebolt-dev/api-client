<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreSituationRequest;
use App\Http\Requests\StoreIncomeRequest;
use App\Http\Requests\StoreExpensesRequest;

use App\Models\Api;
use Carbon\Carbon;

class PageController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        // Flush any sessions
        session()->flush();
        return view('index');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function situation(Request $request)
    {
        return view('calculator.situation', compact('request'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function situation_store(StoreSituationRequest $request)
    {
        $request->session()->put('situation', $request->situation ?? null);
        $request->session()->put('person.0.dateofbirth', (isset($request->person[0]['dateofbirth']) ? Carbon::make($request->person[0]['dateofbirth'])->format('Y-m-d') ?? null : null));
        $request->session()->put('person.1.dateofbirth', $request->person[1]['dateofbirth'] ?? null);

        return redirect()->route('calculator.income');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function income(Request $request)
    {
        return view('calculator.income', compact('request'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function income_store(StoreIncomeRequest $request)
    {
        $request->session()->put('income_type.0', $request->income_type_0 ?? null);
        $request->session()->put('income_type.1', $request->income_type_1 ?? null) ;
        $request->session()->put('person.0.income', (isset($request->person[0]['income']) ? doubleval($request->person[0]['income']) ?? null : null));
        $request->session()->put('person.1.income', (isset($request->person[1]['income']) ? doubleval($request->person[1]['income']) ?? null : null));

        return redirect()->route('calculator.expenses');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function expenses(Request $request)
    {
        return view('calculator.expenses',compact('request'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function expenses_store(StoreExpensesRequest $request)
    {
        $request->session()->put('loans', $request->loans ?? null);

        $request->session()->put('person.0.studentloans', (isset($request->person[0]['studentloans']) ? doubleval($request->person[0]['studentloans']) ?? null : null));
        $request->session()->put('person.0.studentloanstartdate', (isset($request->person[0]['studentloanstartdate']) ? Carbon::make($request->person[0]['studentloanstartdate'])->format('Y-m-d') ?? null : null));
        $request->session()->put('person.0.loans', (isset($request->person[0]['loans']) ? doubleval($request->person[0]['loans']) ?? null : null));
        $request->session()->put('person.0.alimony', (isset($request->person[0]['alimony']) ? doubleval($request->person[0]['alimony']) ?? null : null));

        $request->session()->put('person.1.studentloans', (isset($request->person[1]['studentloans']) ? doubleval($request->person[1]['studentloans']) ?? null : null));
        $request->session()->put('person.1.studentloanstartdate', (isset($request->person[1]['studentloanstartdate']) ? Carbon::make($request->person[1]['studentloanstartdate'])->format('Y-m-d') ?? null : null));
        $request->session()->put('person.1.loans', (isset($request->person[1]['loans']) ? doubleval($request->person[1]['loans']) ?? null : null));
        $request->session()->put('person.1.alimony', (isset($request->person[1]['alimony']) ? doubleval($request->person[1]['alimony']) ?? null : null));

        return redirect()->route('calculator.result');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function result(Request $request)
    {
        $api = new Api();
        $result = $api->calculate($request->session()->all());

        return view('calculator.result',compact('request', 'result'));
    }

}
