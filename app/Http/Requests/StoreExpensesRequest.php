<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreExpensesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loans' => 'required',
            'person.0.studentloanstartdate' => 'required_unless:person.0.studentloans,null',
            'person.1.studentloanstartdate' => 'required_unless:person.1.studentloans,null',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'Je moet een keuze maken.',
            'required_unless' => 'Startdatum is verplicht.',
        ];
    }
}
