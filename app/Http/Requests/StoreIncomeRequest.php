<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreIncomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'income_type_0' => 'required',
            'person.0.income' => 'required',
            'income_type_1' => 'required_if:situation,2',
            'person.1.income' => 'required_if:situation,2',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'Dit veld is verplicht.',
            'required_if' => 'Het inkomen van jouw partner is verplicht.',
        ];
    }
}
