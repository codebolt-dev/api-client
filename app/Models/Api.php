<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

use Illuminate\Support\Arr as Arr;
use Carbon\Carbon;
use phpDocumentor\Reflection\Types\False_;

class Api extends Model
{

    private $url = 'https://api.hypotheekbond.nl/';
    private $api_key;

    public function __construct() {

        $this->api_key = env('API_KEY');
        $this->client = Http::withHeaders([
            'x-api-key' => $this->api_key
        ]);

    }

    //
    public function calculate($data = [])
    {
        // Prepare the data with the correct data types
        $prepared = [];
        $prepared['nhg'] = 'false'; // cast 'false' to a string, because (bool) is casted to 0 in request
        $prepared['private_lease_duration'] = 0;
        $prepared['duration'] = 360;
        $prepared['percentage'] = 1.501;
        $prepared['rateFixation'] = 10;

        // Prepare the data for each person
        foreach($data['person'] as $item) {
            $persons['person'][] = array_filter($item, function ($v, $k) {
                return $v != null;
            }, ARRAY_FILTER_USE_BOTH);
        }
        $prepared['person'] = array_filter($persons['person']);

        // Send the request
        try {
            $response = $this->client->get($this->url . 'calculation/v1/mortgage/maximum-by-income' , $prepared);
            return $response->json();
        } catch(Exception $e) {
            return $e->clientError();
        }
    }
}
