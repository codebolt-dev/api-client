@extends('layout.app')

@section('content')

    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-5">

                <h1 class="h2 mb-3">Maximale hypotheek berekenen</h1>
                <p>Met deze rekentool kun je jouw maximale hypotheek berekenen gebaseerd op jouw situatie, inkomen en uitgaven.</p>

                <a href="{{ route('calculator.situation') }}" class="btn btn-primary" title="">Maximale hypotheek berekenen</a>

            </div>
        </div>
    </div>

@endsection
