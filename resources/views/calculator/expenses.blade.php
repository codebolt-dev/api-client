@extends('layout.app')

@section('content')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">

                <form action="{{ route('calculator.expenses.store') }}" method="post">
                    @csrf

                    <div class="card">

                        <div class="card-header">
                            <h1 class="h2 mt-2">Verplichte uitgaven</h1>
                        </div>

                        <div class="card-body">

                            <div class="mb-3">

                                <label for="loans[no]" class="form-label">Hebben jullie te maken met een studieschuld, partneralimentatie, leningen of kredieten?</label>
                                <div class="form-check">
                                    <input type="radio" name="loans" id="loans[no]" value="0" class="form-check-input"
                                           data-bs-toggle="collapse" data-bs-target="#collapse-situation.show"
                                        {{ Session::get('loans') ? (Session::get('loans') == 0 ? 'checked' : '') : 'checked' }}
                                    />
                                    <label for="loans[no]" class="form-check-label">
                                        Nee
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" name="loans" id="loans[yes]" value="1" class="form-check-input"
                                           data-bs-toggle="collapse" data-bs-target="#collapse-situation:not(.show)"
                                        {{ Session::get('loans') ? (Session::get('loans') == 1 ? 'checked' : '') : '' }}
                                    />
                                    <label for="loans[yes]" class="form-check-label">
                                        Ja
                                    </label>
                                </div>

                            </div>

                            <div id="collapse-situation" class="collapse collapse-situation mt-3 {{ old('loans') == 1 ? 'selected' : Session::get('loans') == 1 ? 'show' : '' }}">
                                <div class="card">
                                    <div class="card-body">

                                        <h3>Studieschuld</h3>

                                        <div class="row mb-3">
                                            <div class="col-6">
                                                <div>
                                                    <label for="person[0][studentloans]" class="form-label">Jouw studieschuld</label>
                                                    <input type="number" id="person[0][studentloans]" name="person[0][studentloans]"
                                                           value="{{ old('person.0.studentloans', Session::get('person.0.studentloans') ?? null) }}"
                                                           class="form-control @error('person.0.studentloans') is-invalid @enderror">

                                                    @error('person.0.studentloans')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div>
                                                    <label for="person[0][studentloanstartdate]" class="form-label">Startdatum van jouw studie</label>
                                                    <input type="date" id="person[0][studentloanstartdate]" name="person[0][studentloanstartdate]"
                                                           value="{{ old('person.0.studentloanstartdate', Session::get('person.0.studentloanstartdate') ?? null) }}"
                                                           class="form-control @error('person.0.studentloanstartdate') is-invalid @enderror">

                                                    @error('person.0.studentloanstartdate')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>

                                        @if(Session::has('situation') && Session::get('situation') == 2)
                                            <div class="row mb-3">
                                                <div class="col-6">
                                                    <div>
                                                        <label for="person[1][studentloans]" class="form-label">Studieschuld van partner</label>
                                                        <input type="number" id="person[1][studentloans]" name="person[1][studentloans]"
                                                               value="{{ old('person.1.studentloans', Session::get('person.1.studentloans') ?? null) }}"
                                                               class="form-control @error('person.1.studentloans') is-invalid @enderror">

                                                        @error('person.1.studentloans')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div>
                                                        <label for="person[1][studentloanstartdate]" class="form-label">Startdatum studie partner</label>
                                                        <input type="date" id="person[1][studentloanstartdate]" name="person[1][studentloanstartdate]"
                                                               value="{{ old('person.1.studentloanstartdate', Session::get('person.1.studentloanstartdate') ?? null) }}"
                                                               class="form-control @error('person.1.studentloanstartdate') is-invalid @enderror">

                                                        @error('person.1.studentloanstartdate')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                </div>
                                            </div>
                                        @endif

                                        <hr>

                                        <h3>Leningen en/of kredieten</h3>
                                        <div class="mb-3">
                                            <label for="person[0][loans]" class="form-label">Jouw schulden of kredieten</label>
                                            <input type="number" id="person[0][loans]" name="person[0][loans]"
                                                   value="{{ old('person.0.loans', Session::get('person.0.loans') ?? null) }}"
                                                   class="form-control @error('person.0.loans') is-invalid @enderror">

                                            @error('person.0.loans')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        @if(Session::has('situation') && Session::get('situation') == 2)

                                            <div class="mb-3">
                                                <label for="person[1][loans]" class="form-label">Schulden of kredieten van jouw partner</label>
                                                <input type="number" id="person[1][loans]" name="person[1][loans]"
                                                       value="{{ old('person.1.loans', Session::get('person.1.loans') ?? null) }}"
                                                       class="form-control @error('person.1.loans') is-invalid @enderror">

                                                @error('person.1.loans')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        @endif

                                        <hr>

                                        <h3>Maandelijkse partneralimentatie</h3>

                                        <div class="mb-3">
                                            <label for="person[0][alimony]" class="form-label">Ik betaal partneralimentatie</label>
                                            <input type="number" id="person[0][alimony]" name="person[0][alimony]"
                                                   value="{{ old('person.0.alimony', Session::get('person.0.alimony') ?? null) }}"
                                                   class="form-control @error('person.0.alimony') is-invalid @enderror">

                                            @error('person.0.alimony')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        @if(Session::has('situation') && Session::get('situation') == 2)
                                            <div class="mb-3">
                                                <label for="person[1][alimony]" class="form-label">Mijn partner betaalt partneralimentatie</label>
                                                <input type="number" id="person[1][alimony]" name="person[1][alimony]"
                                                       value="{{ old('person.1.alimony', Session::get('person.1.alimony') ?? null) }}"
                                                       class="form-control @error('person.1.alimony') is-invalid @enderror">

                                                @error('person.1.alimony')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer justify-content-around">
                            <div class="row">
                                <div class="col-4">
                                    <a href="{{ route('calculator.income') }}" class="btn btn-secondary">Terug</a>
                                </div>
                                <div class="col-8 text-end">
                                    <button class="btn btn-primary">Maximale hypotheek berekenen</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
