@extends('layout.app')

@section('content')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">

                <form action="{{ route('calculator.income.store') }}" method="post">
                    @csrf

                    <div class="card">

                        <div class="card-header">
                            <h1 class="h2 mt-2">Mijn inkomen</h1>
                        </div>

                        <div class="card-body">

                            <div class="mb-3">
                                <label for="income_type[0][employment]" class="form-label">Hoe verdien je je geld?</label>
                                <div class="form-check">
                                    <input type="radio" name="income_type_0" id="income_type[0][employment]" value="1"
                                           class="form-check-input js-income-type"
                                           data-index="0"
                                           data-placeholder="person"
                                           {{ (old('income_type_0') == 1 ? 'checked' : Session::get('income_type.0') == 1 ? 'checked' : 'checked') }} />
                                    <label for="income_type[0][employment]" class="form-check-label">
                                        Loondienst
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input type="radio" name="income_type_0" id="income_type[0][social_benefits]" value="2"
                                           class="form-check-input js-income-type"
                                           data-index="0"
                                           data-placeholder="person"
                                           {{ old('income_type_0') == 2 ? 'checked' : Session::get('income_type.0') == 2 ? 'checked' : '' }} />
                                    <label for="income_type[0][social_benefits]" class="form-check-label">
                                        Uitkering
                                    </label>
                                </div>
                            </div>

                            <div class="mb-3 collapse js-income-form show" data-placeholder-receive="person">
                                @if(Session::has('income_type.0'))
                                    @switch(Session::get('income_type.0'))
                                        @case(1)
                                        @include('calculator.components.form-employment', ['index' => 0])
                                        @break

                                        @case(2)
                                        @include('calculator.components.form-social-benefits', ['index' => 0])
                                        @break
                                    @endswitch
                                @else
                                    @include('calculator.components.form-employment', ['index' => 0])
                                @endif
                            </div>

                            @if(Session::has('situation') && Session::get('situation') == 2)
                                <div class="mb-3">
                                    <label for="income_type[1][employment]" class="form-label">Hoe verdient jouw partner geld?</label>
                                    <div class="form-check">
                                        <input type="radio" name="income_type_1" id="income_type[1][employment]" value="1"
                                               class="form-check-input js-income-type"
                                               data-index="1"
                                               data-placeholder="partner"
                                               {{ old('income_type_1') == 1 ? 'checked' : Session::get('income_type.1') == 1 ? 'checked' : '' }} />
                                        <label for="income_type[1][employment]" class="form-check-label">
                                            Loondienst
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input type="radio" name="income_type_1" id="income_type[1][social_benefits]" value="2"
                                               class="form-check-input js-income-type"
                                               data-index="1"
                                               data-placeholder="partner"
                                               {{ old('income_type_1') == 2 ? 'checked' : Session::get('income_type.1') == 2 ? 'checked' : '' }} />
                                        <label for="income_type[1][social_benefits]" class="form-check-label">
                                            Uitkering
                                        </label>
                                    </div>
                                </div>
                                <div class="mb-3 collapse js-income-form @if(Session::has('income_type.1')) show @endif" data-placeholder-receive="partner">
                                    @if(Session::has('income_type.1'))
                                        @switch(Session::get('income_type.1'))
                                            @case(1)
                                                @include('calculator.components.form-employment', ['index' => 1])
                                            @break

                                            @case(2)
                                                @include('calculator.components.form-social-benefits', ['index' => 1])
                                            @break
                                        @endswitch
                                    @endif
                                </div>
                            @endif
                        </div>

                        <div class="card-footer justify-content-around">
                            <div class="row">
                                <div class="col-4">
                                    <a href="{{ route('calculator.situation') }}" class="btn btn-secondary">Terug</a>
                                </div>
                                <div class="col-8 text-end">
                                    <button class="btn btn-primary">Mijn verplichte uitgaven</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="situation" value="{{ Session::get('situation') ?? 1 }}">
                </form>

            </div>
        </div>
    </div>
@endsection
