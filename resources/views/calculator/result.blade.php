@extends('layout.app')

@section('content')

    <div class="conatiner py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">

                <div class="card">

                    <div class="card-header">
                        <h1 class="h2 mt-2">Jouw maximale hypotheek</h1>
                    </div>

                    <div class="card-body">

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="row">
                                    <div class="col-12">
                                        <h4 class="mb-0">Wat je kunt lenen</h4>
                                        <p class="h2 text-primary">&euro; {{ number_format($result['data']['result'], 0, '', '.') }}</p>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <h4>Je bruto maandlasten</h4>
                                        <p>&euro; {{ number_format($result['data']['calculationValues']['firstGrossPayment'], 0, '', '.') }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6">
                                <div class="row">
                                    <div class="col-12">
                                        <h4 class="mb-0">Je inkomsten en uitgaven</h4>
                                        <span class="h2 text-primary">&euro; {{ $result['data']['calculationValues']['totalReferenceIncome'] }}</span>
                                    </div>
                                    <div class="col-12 mt-3">
                                        <h4>Jouw rente</h4>
                                        <p>Rente 1,501%</p>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-12">
                                <a href="{{ route('index') }}" class="btn btn-primary">Opnieuw berekenen</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
