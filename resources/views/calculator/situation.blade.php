@extends('layout.app')

@section('content')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">


                <form action="{{ route('calculator.situation.store') }}" method="post">
                    @csrf

                    <div class="card">
                        <div class="card-header">
                            <h1 class="h2 mt-2">Mijn situatie</h1>
                        </div>
                        <div class="card-body">

                            <div class="mb-3">
                                <label for="situation[alone]" class="form-label">Koop je alleen of samen?</label>
                                <div class="form-check">
                                    <input type="radio"
                                           name="situation"
                                           id="situation[alone]"
                                           class="form-check-input"
                                           value="1"
                                           data-bs-toggle="collapse" data-bs-target="#collapse-situation.show" checked
                                           {{ old('situation') == 1 ? 'checked' : Session::get('situation') == 1 ? 'checked' : '' }}
                                    />
                                    <label for="situation[alone]" class="form-check-label">
                                        Alleen
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input type="radio"
                                           name="situation"
                                           id="situation[together]"
                                           class="form-check-input"
                                           value="2"
                                           data-bs-toggle="collapse" data-bs-target="#collapse-situation:not(.show)"
                                           {{ old('situation') == 2 ? 'checked' : Session::get('situation') == 2 ? 'checked' : '' }}
                                    />
                                    <label for="situation[together]" class="form-check-label">
                                        Samen
                                    </label>
                                </div>

                                @error('situation')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="person[0][dateofbirth]" class="form-label">Wat is je geboortedatum?</label>
                                <input type="date" id="person[0][dateofbirth]" name="person[0][dateofbirth]"
                                       value="{{ old('person.0.dateofbirth', Session::get('person.0.dateofbirth') ?? null) }}"
                                       class="form-control @error('person.0.dateofbirth') is-invalid @enderror" required>

                                @error('person.0.dateofbirth')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div id="collapse-situation"
                                class="collapse collapse-situation mt-3 js-collapse-situation {{ old('situation') == 2 ? 'selected' : Session::get('situation') == 2 ? 'show' : '' }}">
                                <label for="person[1][dateofbirth]" class="form-label">Wat is de geboortedatum van jouw partner?</label>
                                <input type="date" id="person[1][dateofbirth]" name="person[1][dateofbirth]"
                                       value="{{ old('person.1.dateofbirth', Session::get('person.1.dateofbirth') ?? null) }}"
                                       class="form-control @error('person.1.dateofbirth') is-invalid @enderror">
                            </div>
                            @error('person.1.dateofbirth')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>

                        <div class="card-footer justify-content-around">
                            <div class="row">
                                <div class="col-12 text-end">
                                    <button class="btn btn-primary">Mijn inkomen</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
