<div>
    <h3>{{ ($index == 0 ? 'Jouw salaris' : 'Saris van jouw partner') }}</h3>
    <div class="mb-3">
        <label for="person[{{ $index }}][income]" class="form-label">{{ ($index == 0 ? 'Wat verdien je bruto per jaar?' : 'Wat verdient jouw partner bruto per jaar?') }}</label>
        <input type="number" id="person[{{ $index }}][income]" name="person[{{ $index }}][income]"
               value="{{ old('person.' . $index . '.income', Session::get('person.' . $index . '.income') ?? null) }}"
               class="form-control"> <!-- removed the required for testing -->

        @error('person.' . $index . '.income')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
