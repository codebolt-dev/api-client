<div>
    <h3>{{ ($index == 0 ? 'Jouw uitkering' : 'Uitkering van jouw partner') }}</h3>
    <div class="mb-3">
        <label for="person[{{ $index }}][income]" class="form-label">{{ ($index == 0 ? 'Hoe hoog is jouw uitkering bruto per jaar?' : 'Hoe hoog is de uitkering van jouw partner per jaar?') }}</label>
        <input type="number" id="person[{{ $index }}][income]" name="person[{{ $index }}][income]"
               value="{{ old('person.' . $index . '.income', Session::get('person.' . $index . '.income') ?? null) }}"
               class="form-control" required>

        @error('person.' . $index . '.income')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
