require('./bootstrap');

import $ from 'jquery';

import {Collapse} from 'bootstrap';

var App = App || {};

$(function () {
    App.incomeType();
});


/**
 *
 */
App.incomeType = function() {

    $('.js-income-type').change(function(e) {

        var placeholder = $(this).data('placeholder');

        $.ajax({
            url: '/data/income_type',
            type: 'POST',
            data: {
                "_token": $('input[name="_token"]').val(),
                "income_type" : $(this).val(),
                "index" : $(this).data('index')
            },
            success: function (data) {
                $('.js-income-form[data-placeholder-receive="' + placeholder + '"]').html(data);
                $('.js-income-form[data-placeholder-receive="' + placeholder + '"]').addClass('show');
            }
        });

    });

}
