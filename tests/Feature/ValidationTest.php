<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class ValidationTest extends TestCase
{
    /**
     * Check POST to /situatie
     * Assert that the request can be made and the user will be redirected to the next form
     */
    public function testPostToSituation()
    {

        $response = $this->call('POST', route('calculator.situation.store'), array(
            '_token' => csrf_token(),
            'situation' => 1,
            'person' => [
                [
                    'dateofbirth' => Carbon::make('today - 35 years')->format('Y-m-d'),
                ]
            ],
        ));

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('calculator.income'));

    }

    /**
     * Check POST to /inkomen
     * Assert that the request can be made and the user will be redirected to the next form
     */
    public function testPostToIncome()
    {

        $response = $this->call('POST', route('calculator.income.store'), array(
            '_token' => csrf_token(),
            'income_type_0' => 1,
            'person' => [
                [
                    'income' => 60000,
                ]
            ],
        ));

        $response->assertSessionHasNoErrors();
        $response->assertRedirect(route('calculator.expenses'));

    }

    /**
     * Check POST to /uitgaven
     * Assert that the request can be made, but there are validation errors
     */
    public function testPostToExpenses()
    {

        $response = $this->call('POST', route('calculator.expenses.store'), array(
            '_token' => csrf_token(),
            // 'loans' => false, // Loans is required, but we do not send this
        ));

        $response->assertSessionHasErrors();
    }
}
