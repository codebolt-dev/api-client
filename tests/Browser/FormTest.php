<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class FormTest extends DuskTestCase
{
    /**
     * Test if a user can fill in the form in order to buy a house alone
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit(env('APP_URL', 'http://api-client.test/'))
                    ->assertSee('Maximale hypotheek berekenen')
                    ->click('.btn.btn-primary');

            // Situation
            $browser->radio('situation', '1');
            $browser->type('person[0][dateofbirth]', '05-11-1987');
            $browser->screenshot('step-1-completed');
            $browser->press('.btn.btn-primary');

            // Income
            $browser->radio('income_type_0', '1');
            $browser->press('.btn.btn-primary')
                ->assertSee('Dit veld is verplicht.'); // see an error (disabled the required for this field)

            $browser->type('person[0][income]', '60000');
            $browser->screenshot('step-2-completed');
            $browser->press('.btn.btn-primary');

            // Expenses
            $browser->radio('loans', '0');
            $browser->screenshot('step-3-completed');
            $browser->press('.btn.btn-primary');

            // Result (this part of the test is creating a call to the API)
            // $browser->assertSee('Jouw maximale hypotheek');
            // $browser->screenshot('step-4-completed');
        });
    }
}
