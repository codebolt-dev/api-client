<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PageController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\DataController;

Route::get('/', [PageController::class, 'index'])->name('index');

Route::get('/situatie', [PageController::class, 'situation'])->name('calculator.situation');
Route::post('/situatie', [PageController::class, 'situation_store'])->name('calculator.situation.store');

Route::get('/inkomen', [PageController::class, 'income'])->name('calculator.income');
Route::post('/inkomen', [PageController::class, 'income_store'])->name('calculator.income.store');

Route::get('/uitgaven', [PageController::class, 'expenses'])->name('calculator.expenses');
Route::post('/uitgaven', [PageController::class, 'expenses_store'])->name('calculator.expenses.store');

Route::get('/maximale-hypotheek', [PageController::class, 'result'])->name('calculator.result');


// API routes
//Route::post('/calculate', [ApiController::class, 'calculate'])->name('calculate');

// Data
Route::post('/data/income_type', [DataController::class, 'income_type']);
